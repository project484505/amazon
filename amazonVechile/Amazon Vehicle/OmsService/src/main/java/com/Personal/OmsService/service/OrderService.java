package com.Personal.OmsService.service;

public interface OrderService {
    int placeOrder();
    void validatePayment();
}
