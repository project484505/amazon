package com.Personal.OmsService.Kafka;

import ch.qos.logback.core.encoder.JsonEscapeUtil;
import com.Personal.OmsService.Utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

@Configuration
public class KafkaProducerConfig {

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    public void publish(String topicName, Object object){
        //converting object to JSON
        String json = JsonUtils.toJson(object);
        kafkaTemplate.send(topicName,json);
    }
}
