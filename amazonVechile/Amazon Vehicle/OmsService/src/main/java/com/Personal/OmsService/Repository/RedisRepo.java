package com.Personal.OmsService.Repository;

import java.util.List;

public interface RedisRepo {

    Object save(Object object, String id);

    List<Object> findAll();

    Object findProductById(String id);

    String DeleteProductEntity(int Id);
}
