package com.Personal.OmsService.Repository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Slf4j
public class RedisRepoImpl implements RedisRepo{

    private static final Object HASH_KEY = "OmsRedisKey";
    private final RedisTemplate redisTemplate;

    public RedisRepoImpl(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Object save(Object object, String id){
        redisTemplate.opsForHash().put(HASH_KEY, id, object);
        return object;
    }

    @Override
    public List<Object>findAll(){
        return redisTemplate.opsForHash().values(HASH_KEY);
    }

    @Override
    public Object findProductById(String id){
        try {
            return (Object) redisTemplate.opsForHash().get(HASH_KEY, id);
        }
        catch (Exception e){
            log.info("Getting exception: {}", e);
            throw new RuntimeException("getting exception in findProductById");
        }
    }

    @Override
    public String DeleteProductEntity(int Id){
        redisTemplate.opsForHash().delete(HASH_KEY,"1");
        return "product remove";

    }

}
