package com.Personal.OmsService.controller;

import com.Personal.OmsService.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderApi {

    // injecting
    @Autowired
    private final OrderService orderService;

    public OrderApi(OrderService orderService) {

        this.orderService = orderService;
    }

    @GetMapping()
     public int placeOrder(){
         return orderService.placeOrder();
     }
}
