package com.Personal.OmsService;

import com.Personal.OmsService.Utils.AppConstants;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.kafka.config.TopicBuilder;

@SpringBootApplication
@EnableFeignClients
@Configuration
@EnableRedisRepositories
public class OmsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OmsServiceApplication.class, args);
	}

	@Bean
	public LettuceConnectionFactory redisStandAloneConnectionFactory() {
		return new LettuceConnectionFactory(new RedisStandaloneConfiguration("127.0.0.1", 6379));
	}

	@Bean
	public RedisTemplate<String,Object> redisTemplate(){
		RedisTemplate<String,Object>template = new RedisTemplate<>();
		template.setConnectionFactory(redisStandAloneConnectionFactory());
		template.setKeySerializer(new StringRedisSerializer());
		template.setHashKeySerializer(new StringRedisSerializer());
		template.setHashKeySerializer(new JdkSerializationRedisSerializer());
		template.setValueSerializer(new JdkSerializationRedisSerializer());
		template.setEnableTransactionSupport(true);
		template.afterPropertiesSet();
		return template;
	}

	@Bean
	public NewTopic locationTopic(){
		return TopicBuilder
				.name(AppConstants.LOCATION_TOPIC_NAME)
				.build();
	}

	@Bean
	public NewTopic orderTopic(){
		return TopicBuilder
				.name(AppConstants.ORDER_TOPIC_NAME)
				.build();
	}
}
