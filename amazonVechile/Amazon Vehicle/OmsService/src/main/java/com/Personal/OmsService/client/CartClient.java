package com.Personal.OmsService.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "cartServiceClient", url = "http://127.0.0.1:8085/cart")
public interface CartClient {

    @GetMapping("/fetch")
    public int getId();
}
