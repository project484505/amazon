package com.Personal.OmsService.service;

import com.Personal.OmsService.Kafka.KafkaProducerConfig;
import com.Personal.OmsService.Repository.RedisRepo;
import com.Personal.OmsService.Utils.AppConstants;
import com.Personal.OmsService.Utils.JsonUtils;
import com.Personal.OmsService.client.CartClient;
import com.Personal.OmsService.common.Car;
import com.Personal.OmsService.common.Vehicle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService{

    //Inject
    private final CartClient cartClient;
    private final RedisRepo redisRepo;

    private KafkaProducerConfig kafkaProducerConfig = new KafkaProducerConfig();

    // External client
    // database repo
    // cache repo
    // kafka repo

    public <kafkaProducerConfig> OrderServiceImpl(CartClient cartClient, RedisRepo redisRepo, KafkaProducerConfig kafkaProducerConfig){
        this.cartClient = cartClient;
        this.redisRepo = redisRepo;
        this.kafkaProducerConfig = kafkaProducerConfig;
    }

    @Override
    public int placeOrder() {
//        int id = cartClient.getId();
        int id = 10;
        redisRepo.save(id, String.valueOf(id));

        Car car = new Car(6656, 2024, "BMW");
        log.info("getting card data:{}",car);

        Vehicle vehicle = Vehicle.builder().model("TATA Nexon").type("Sports").year("2020").build();

        log.info("getting redis cache data: {}", redisRepo.findProductById(String.valueOf(id)));

        kafkaProducerConfig.publish(AppConstants.ORDER_TOPIC_NAME, car);
        kafkaProducerConfig.publish(AppConstants.LOCATION_TOPIC_NAME, vehicle);
//        return cartClient.getId();
        return id;
    }

    @Override
    public void validatePayment() {

    }
}
