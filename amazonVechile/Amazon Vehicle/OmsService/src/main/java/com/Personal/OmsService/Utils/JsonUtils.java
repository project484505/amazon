package com.Personal.OmsService.Utils;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Optional;

@Slf4j
public class JsonUtils {

    // Object type can accept ---> any data set ( example int, string, any class object etc..)
    public static String toJson(Object obj) {
        String json = "";
        if (obj != null) {
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                json = objectMapper.writeValueAsString(obj);
            } catch (JsonProcessingException e) {
                log.info("error while converting object : {} to string : {}", obj, e);
            }
        }
        return json;
    }



    public static <T> T fromJson(String json, Class<T> clazz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json, clazz);
        } catch (IOException e) {
            log.error("error while creating object of {}, {}", clazz.getName(), e);
        }
        return null;
    }


//    public static <I, O> O copyOrTransform(I value, Class<O> oClass) throws IOException {
//        byte[] intermediate = MAPPER.writeValueAsBytes(value);
//        return MAPPER.readValue(intermediate, oClass);
//    }
//
//    public static <T> T fromPath(String path) throws IOException, URISyntaxException {
//        URL resource = JsonUtil.class.getClassLoader().getResource(path);
//        assert resource != null;
//        byte[] src = Files.readAllBytes(Paths.get(resource.toURI()));
//        return (T) MAPPER.readValue(src, EncodedPdfDto.class);
//    }
//
//    public static <T> T fromPath(String path, TypeReference<T> type) throws IOException, URISyntaxException {
//        URL resource = JsonUtil.class.getClassLoader().getResource(path);
//        assert resource != null;
//        byte[] src = Files.readAllBytes(Paths.get(resource.toURI()));
//        return MAPPER.readValue(src, type);
//    }
}