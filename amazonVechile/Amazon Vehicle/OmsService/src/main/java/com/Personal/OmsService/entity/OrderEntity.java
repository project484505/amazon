package com.Personal.OmsService.entity;


import jakarta.persistence.*;
import lombok.Data;
import org.springframework.context.annotation.Configuration;

@Data
@Entity
@Table(name = "oms")
public class OrderEntity {
    @Id
    @GeneratedValue

    @Column(name = "id")
    private  int id;

    @Column (name = "productName")
    private String productName;



}
