package com.Personal.OmsService.Utils;

public class AppConstants {
    public static final String LOCATION_TOPIC_NAME = "Location-update-topic";

    public static final String TIME_TOPIC_NAME = "time-update-topic";
    public static final String GROUP_ID = "group-oms-v-2";
    public static final String ORDER_TOPIC_NAME = "Order-update-topic";
    public static final String ORDER_STATUS_TOPIC_NAME = "Order-status-update-topic";
}
