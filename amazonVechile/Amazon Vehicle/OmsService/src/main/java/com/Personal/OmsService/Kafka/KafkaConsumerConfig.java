package com.Personal.OmsService.Kafka;

import com.Personal.OmsService.Utils.AppConstants;
import com.Personal.OmsService.Utils.JsonUtils;
import com.Personal.OmsService.common.Car;
import com.Personal.OmsService.common.Vehicle;
import com.Personal.OmsService.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

@Slf4j
@Configuration
public class KafkaConsumerConfig {

    private final OrderService orderService;

    public KafkaConsumerConfig(OrderService orderService) {

        this.orderService = orderService;
    }

    @KafkaListener(topics = AppConstants.LOCATION_TOPIC_NAME,groupId=AppConstants.GROUP_ID)
    public void updateLocation(String value){
        orderService.validatePayment();
        log.info("Successfully received Topic in consumer: {}{}", JsonUtils.fromJson(value, Vehicle.class), AppConstants.LOCATION_TOPIC_NAME);
    }

    @KafkaListener(topics = AppConstants.ORDER_TOPIC_NAME,groupId=AppConstants.GROUP_ID)
    public void orderLocation(String value){
        log.info("Successfully received Topic in consumer: {}{}", JsonUtils.fromJson(value, Car.class), AppConstants.ORDER_TOPIC_NAME);
    }
}
