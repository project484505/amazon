package com.CartService.cartService.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "CartService", url = "http://127.0.0.1:8088/omsService")
public interface OmsClient {

    @GetMapping("/cartId")
    public int getcartId();

    @GetMapping("/name")
    public String getname();
}
