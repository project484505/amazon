package com.ConfigrationService.ConfigrationService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConfigrationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigrationServiceApplication.class, args);
	}

}
